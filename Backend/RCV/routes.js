import { Router } from "express";
import {
  getBevoelkerung,
  getGeschlecht,
  getStaat,
  getMap,
  getGemeinden
} from "./controller.js";

const router = Router();

router.get("/bev/:gemeindeNr", getBevoelkerung);
router.get("/ges/:gemeindeNr", getGeschlecht);
router.get("/staat/:gemeindeNr/:jahr", getStaat);
router.get("/map/:gemeindeNr", getMap);
router.get("/getGemeinden",getGemeinden)

export { router };
