import axios from "axios"; //axios wird importiert

async function getBevoelkerung(req, res) {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json")
    .then((response) => {
      //daten werden als JSON geholt
      const json = response.data;
      //daten werden an die constante json übergeben
      let returnArray = [];
      //die rückgabewerte werden in returnArray als java Objekt gespeichert

      for (let x = 0; x < Object.keys(json).length; x++) {
        //man geht einzeln durch das ganze json file
        if (req.params.gemeindeNr == json[x].bfs_nr_gemeinde) {
          //es wird geprüft ob diese stelle des Json objekts die gleiche gemeindeNr hat
          if (Object.keys(returnArray).length == 0) {
            //bei gleicher gemeindenummer wird
            returnArray.push({
              //die passenden daten werden in returnArray gepusht
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              anzahl_personen: parseInt(json[x].anzahl_personen),
              jahr: parseInt(json[x].jahr),
            });
          }
          let kommtVor = false; //zur überprüfung ob der Datensatz schon gespeichert wird
          for (let y = 0; y < Object.keys(returnArray).length; y++) {
            if (parseInt(json[x].jahr) == returnArray[y].jahr) {
              //wenn das Jahr vorhanden ist wird dazugezählt
              returnArray[y].anzahl_personen += parseInt(
                json[x].anzahl_personen
              );
              kommtVor = true; //somit wird verhindert dass für das jahr ein neuer datensatz erstellt wird
            }
          }
          if (!kommtVor) {
            //wenn oben nicht festgestellt wird dass das Jahr existiert geht es in diese if-abfrage
            returnArray.push({
              //es wird ein neuer Datensatz gepusht
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              anzahl_personen: parseInt(json[x].anzahl_personen),
              jahr: parseInt(json[x].jahr),
            });
          }
        }
      }
      res.send(returnArray); //das resultat wird gesendet
    });
}

async function getGeschlecht(req, res) {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json")
    .then((response) => {
      //daten werden als JSON geholt
      const json = response.data;
      //daten werden an die constante json übergeben
      let returnArray = [];
      //die rückgabewerte werden in returnArray als java Objekt gespeichert
      for (let x = 0; x < Object.keys(json).length; x++) {
        //man geht einzeln durch das ganze json file
        if (req.params.gemeindeNr == json[x].bfs_nr_gemeinde) {
          //es wird geprüft ob diese stelle des Json objekts die gleiche gemeindeNr hat
          if (Object.keys(returnArray).length == 0) {
            //neuer Datensatz gepusht falls nicht vorhanden
            returnArray.push({
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              jahr: parseInt(json[x].jahr),
              geschlecht_code: parseInt(json[x].geschlecht_code),
              anzahl_personen: parseInt(json[x].anzahl_personen),
            });
          }
          let kommtVor = false; //zur überprüfung ob der Datensatz schon gespeichert wird

          for (let y = 0; y < returnArray.length; y++) {
            if (
              parseInt(json[x].jahr) == returnArray[y].jahr &&
              parseInt(json[x].geschlecht_code) ==
                returnArray[y].geschlecht_code
            ) {
              //wenn das Jahr und Geschlecht vorhanden ist wird dazugezählt
              returnArray[y].anzahl_personen += parseInt(
                json[x].anzahl_personen
              );
              kommtVor = true; //verhindert dass immer neuer datensatz erstellt wird
            }
          }
          if (!kommtVor) {
            //falls Jahr nicht vorhanden hinzufügen
            returnArray.push({
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              jahr: parseInt(json[x].jahr),
              geschlecht_code: parseInt(json[x].geschlecht_code),
              anzahl_personen: parseInt(json[x].anzahl_personen),
            });
          }
        }
      }
      res.send(returnArray); //das resultat wird gesendet
    });
}

async function getStaat(req, res) {
  axios
      .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json")
      .then((response) => {
        //daten werden als JSON geholt
        const json = response.data;
        //daten werden an die constante json übergeben
        let returnArray1 = [];
        let returnArray2 = [];
        //die rückgabewerte werden in returnArray als java Objekt gespeichert
        for (let x = 0; x < json.length; x++) {
          //man geht einzeln durch das ganze json file
          if (
              req.params.gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
              req.params.jahr == parseInt(json[x].jahr) &&
              parseInt(json[x].nationalitaet_code) == 1
              //abfrage nach gemeindeNr Jahr und Nationalität
          ) {
            returnArray1.push({
              //daten pushen
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              jahr: parseInt(json[x].jahr),
              nationalitaet_code: parseInt(json[x].nationalitaet_code),
              nationalitaet_bezeichnung: json[x].nationalitaet_bezeichnung,
              geschlecht_code: parseInt(json[x].geschlecht_code),
              geschlecht_bezeichnung: json[x].geschlecht_bezeichnung,
              anzahl_personen: parseInt(json[x].anzahl_personen),
            });
          } else if (
              req.params.gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
              req.params.jahr == parseInt(json[x].jahr) &&
              parseInt(json[x].nationalitaet_code) == 2
              //abfrage nach gemeindeNr Jahr und Nationalität
          ) {
            returnArray2.push({
              //daten pushen
              bfs_nr_gemeinde: parseInt(json[x].bfs_nr_gemeinde),
              gemeinde_name: json[x].gemeinde_name,
              jahr: parseInt(json[x].jahr),
              nationalitaet_code: parseInt(json[x].nationalitaet_code),
              nationalitaet_bezeichnung: json[x].nationalitaet_bezeichnung,
              geschlecht_code: parseInt(json[x].geschlecht_code),
              geschlecht_bezeichnung: json[x].geschlecht_bezeichnung,
              anzahl_personen: parseInt(json[x].anzahl_personen),
            });
          }
        }
        returnArray1[2] = returnArray2[0];
        returnArray1[3] = returnArray2[1];
        res.send(returnArray1); //das resultat wird gesendet
      });
}

async function getMap(req, res) {
  axios
    .get("https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json")
    .then(async (response) => {
      //daten werden als JSON geholt
      const json = response.data;
      //daten werden an die constante json übergeben
      let returnArray = [];
      //die rückgabewerte werden in returnArray als java Objekt gespeichert
      let spot;
      //für stelle des datensatzes von gemeinde
      let rueckgabe = await mapBevoelkerung(req.params.gemeindeNr);
      //resultat von mapBevölkerung
      for (let x = 0; x < Object.keys(json).length; x++) {
        //man geht einzeln durch das ganze json file
        if (
          req.params.gemeindeNr == json[x].bfs_nr_gemeinde &&
          json[x].jahr == 2022
        ) {
          //es wird geprüft ob diese stelle des Json objekts die gleiche gemeindeNr hat
          spot = x;
        }
      }
      rueckgabe = Object.values(rueckgabe[0]).map((r) => parseInt(r));
      //macht es zu einem array mit integern
      returnArray.push({
        bfs_nr_gemeinde: parseInt(json[spot].bfs_nr_gemeinde),
        gemeinde_name: json[spot].gemeinde_name,
        bezirk_name: json[spot].bezirk_name,
        jahr: parseInt(json[spot].jahr),
        anzahl_personen_total:
          rueckgabe[0] + rueckgabe[1] + rueckgabe[2] + rueckgabe[3],
        anzahl_personen_männlich: rueckgabe[0] + rueckgabe[2],
        anzahl_personen_weiblich: rueckgabe[1] + rueckgabe[3],
        anzahl_personen_männlich_schweiz: rueckgabe[0],
        anzahl_personen_weiblich_schweiz: rueckgabe[1],
        anzahl_personen_männlich_ausland: rueckgabe[2],
        anzahl_personen_weiblich_ausland: rueckgabe[3],
      });
      res.send(returnArray); //das resultat wird gesendet
    });
}

async function mapBevoelkerung(gemeindeNr) {
  console.log("mapBevoelkerung");
  const response = await axios.get(
    "https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json"
  );
  console.log("mapB");
  //daten werden als JSON geholt
  const json = response.data;
  //daten werden an die constante json übergeben
  let returnArray = [];
  //die rückgabewerte werden in returnArray als java Objekt gespeichert
  let spottingArray = [];
  //lässt wissen wo die einzelen datensätze sind
  for (let x = 0; x < json.length; x++) {
    if (
      //suche für männlich schweizer
      gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
      parseInt(json[x].geschlecht_code) == 1 &&
      parseInt(json[x].nationalitaet_code) == 1
    ) {
      spottingArray[0] = x; //speichert die stelle des gefundenen
    } else if (
      //suche für weiblich schweizer
      gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
      parseInt(json[x].geschlecht_code) == 2 &&
      parseInt(json[x].nationalitaet_code) == 1
    ) {
      spottingArray[1] = x; //speichert die stelle des gefundenen
    } else if (
      //suche für männlich ausländer
      gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
      parseInt(json[x].geschlecht_code) == 1 &&
      parseInt(json[x].nationalitaet_code) == 2
    ) {
      spottingArray[2] = x; //speichert die stelle des gefundenen
    } else if (
      //suche für weiblich ausländer
      gemeindeNr == parseInt(json[x].bfs_nr_gemeinde) &&
      parseInt(json[x].geschlecht_code) == 2 &&
      parseInt(json[x].nationalitaet_code) == 2
    ) {
      spottingArray[3] = x; //speichert die stelle des gefundenen
    }
  }
  returnArray.push({
    //pusht resultat
    anzahl_personen_männlich_schweiz: json[spottingArray[0]].anzahl_personen,
    anzahl_personen_weiblich_schweiz: json[spottingArray[1]].anzahl_personen,
    anzahl_personen_männlich_ausland: json[spottingArray[2]].anzahl_personen,
    anzahl_personen_weiblich_ausland: json[spottingArray[3]].anzahl_personen,
  });
  return returnArray; //das resultat wird gesendet
}

async function getGemeinden(req,res) {
  const response = await axios.get(
      "https://data.tg.ch/api/v2/catalog/datasets/sk-stat-59/exports/json"
  ); //macht eine Abfrage bei der API
  const json = response.data; //holt die nötigen Daten im JSON format heraus.
  let gemeindeArr = []; //Array deklarieren welcher am schluss zurügegeben werden sollte
  let alreadyIn = false; //Boolean welcher prüft ob das Element schon vorkommt, standarthaft ist dieser auf false gestellt da zu beginn noch keine Gemeinde im Array ist;
  for (let i = 0; i < json.length; i++) { // er geht durch jedes Objekt im Array durch
    for (let j = 0; j < gemeindeArr.length; j++) { // er geht durch den ganzen bisherigen Array mit Gemeinden durch
      if (gemeindeArr[j].bfs_nr_gemeinde === json[i].bfs_nr_gemeinde) { //Prüft jedes Element des GemeindenArrays ob es die gleiche Nummer hat wie das vom aktuellen Array welchen wir holten.
        alreadyIn = true; //Falls Ja wissen wir das diese Gemeinde schon vorkommt, wir setzen den Boolean true
      }
    }
    if (!alreadyIn) { // Wir prüfen ob das Element noch nicht vorkommt. Wennn es nicht vorkommt fügen wir es hinzu
      gemeindeArr.push({
        bfs_nr_gemeinde: json[i].bfs_nr_gemeinde,
        gemeinde_name: json[i].gemeinde_name,
      });
    }
    alreadyIn = false; // Wir stellen den Wert wieder auf false da wir davon ausgehen das das nächste Element auch noch nicht vorhanden ist, erst wenn es bewiesen wird das es schon vorkommt wird es dann auf true gestellt
  }

  res.json(gemeindeArr); // Der Array mit den Gemeinden wird zurückgegeben
}


export { getBevoelkerung, getGeschlecht, getStaat, getMap, getGemeinden }; //die Funktionen werden exportiert
