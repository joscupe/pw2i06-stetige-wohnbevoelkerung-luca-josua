import express from "express";
import { router as routes } from "./RCV/routes.js";

const app = express();
app.use(express.static("./frontend/"))
app.use(express.json());
app.use("/api/RCV", routes);


app.listen("3001", async () => {
  console.log("server listens to 3001");
});
