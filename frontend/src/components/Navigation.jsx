
function Navigation() {
  return (
    <div className={""}>
      <nav className="bg-gray-50 dark:bg-gray-700">
        <div className="max-w-screen-xl px-4 py-3 mx-auto">
          <div className="flex items-center">
            <ul className="flex flex-row font-medium mt-0 mr-6 space-x-8 text-sm">
              <li>
                <a
                  href="#map"
                  className="text-gray-900 dark:text-white hover:underline"
                  aria-current="page"
                >
                  Karte
                </a>
              </li>
              <li>
                <a
                  href="#staat"
                  className="text-gray-900 dark:text-white hover:underline"
                >
                  Staatsbürgerschaft
                </a>
              </li>
              <li>
                <a
                  href="#entwicklung"
                  className="text-gray-900 dark:text-white hover:underline"
                >
                  Bevölkerungszahl total
                </a>
              </li>
              <li>
                <a
                  href="#geschlecht"
                  className="text-gray-900 dark:text-white hover:underline"
                >
                  Geschlecht
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navigation;
