import React from "react";
function Introduction() {
  return (
    <div>
      <h1 className="flex justify-start text-4xl mb-8">
        Ständige Wohnbevölkerung nach Gemeinde, Geschlecht und
        Staatsangehörigkeit
      </h1>
      <p className="flex justify-start text-base">
        Diese Seite Stellt verschiedene Statistiken dar. Eine interaktive Karte
        bei welcher man eine Gemeinde anklicken kann und dann mehr informationen
        über diese bekommt. Weiter hat es 3 Diagramme. Das Kreisdiagramm stellt
        die Staatsangehörigkeit der ausgewählten Gemeinde zum ausgewählten Jahr
        dar. Das Liniendiagramm zeigt den Verlauf der Einwohner in der
        ausgewählten Gemeinde und das Säulendiagramm zeigt den Verlauf der
        Bevälkerungszahl Männlicher und Weiblicher Bürger.
      </p>
    </div>
  );
}

export default Introduction;
