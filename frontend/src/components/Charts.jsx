import {useEffect, useState} from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend, CategoryScale, LinearScale, Title, PointElement, LineElement, BarElement } from "chart.js";
import { Pie, Line, Bar } from "react-chartjs-2";
import {createGemeindeArr } from "./Map.jsx";
import axios from "axios";

ChartJS.register(ArcElement, Tooltip, Legend, CategoryScale,LinearScale, PointElement,LineElement,Title, BarElement);

//Standarwert setzen damit die Diagramme geladen werden
function createComboBox(setJahr, setPieValue, setBfs_nr_gemeinde, bfs_nr_gemeinde, jahr, setGemeinde) { //ComboBox erstellen
  console.log("create");
  axios.get("/api/RCV/getGemeinden").then((res) => {
    let gemeindeArr = res.data;
    console.log(gemeindeArr)
    let elJahre = document.getElementById("jahre"); //Range Slider konfigurieren
    elJahre.addEventListener("change", async (e) =>{ //Jahreszahl anpassen
      await setJahr(e.currentTarget.value);
    });
    let elSelect = document.querySelector("#gemeindeCombo") //ComboBox auswählen
    gemeindeArr.sort((a, b) => //Gemeinde Array sortieren
      a.gemeinde_name < b.gemeinde_name ? -1 : a.gemeinde_name > b.gemeinde_name ? 1 : 0
    )
    for(let i = 0; i < gemeindeArr.length; i++) { // Geht durch den Gemeinde Array durch
      let elOption = document.createElement("option"); //erstellt ein Option element
      elOption.setAttribute("value", gemeindeArr[i].gemeinde_name); //Value zu gemeinde Namen setzen
      elOption.setAttribute("id", gemeindeArr[i].bfs_nr_gemeinde); //Id zu bfs_nr setzend
      elOption.textContent = gemeindeArr[i].gemeinde_name; //Inhalt zu gemeinde Namen setzen
      if(elOption.id == 4401) { //To set Arbon as default selected
        elOption.selected = true;
      }

      elSelect.appendChild(elOption);
    }
    elSelect.addEventListener("change", async (e) => { //Cahnge Event
      console.log("CHANGE");
      let elSelected = document.querySelector("#gemeindeCombo option:checked")
      await setBfs_nr_gemeinde(elSelected.id)
      await setGemeinde(elSelected.value);
      // Values anpassen
    })
  })

}

function Charts() { //Komponent welcher alles mit den Diagrammen regelt
  const [jahr, setJahr] = useState(2022);
  const [bfs_nr_gemeinde, setBfs_nr_gemeinde] = useState(4401);
  const [gemeinde, setGemeinde] = useState("Arbon")
  const [lineValue, setLineValue] = useState([{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":17873,"jahr":2022},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":15123,"jahr":2021},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14872,"jahr":2020},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14718,"jahr":2019},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14533,"jahr":2018},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14430,"jahr":2017},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14256,"jahr":2016},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","anzahl_personen":14184,"jahr":2015}])
  const [barValue, setBarValue] = useState([{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"geschlecht_code":2,"anzahl_personen":10146},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"geschlecht_code":1,"anzahl_personen":7727},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2021,"geschlecht_code":1,"anzahl_personen":7563},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2021,"geschlecht_code":2,"anzahl_personen":7560},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2020,"geschlecht_code":1,"anzahl_personen":7454},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2020,"geschlecht_code":2,"anzahl_personen":7418},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2019,"geschlecht_code":2,"anzahl_personen":7339},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2019,"geschlecht_code":1,"anzahl_personen":7379},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2018,"geschlecht_code":1,"anzahl_personen":7268},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2018,"geschlecht_code":2,"anzahl_personen":7265},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2017,"geschlecht_code":2,"anzahl_personen":7232},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2017,"geschlecht_code":1,"anzahl_personen":7198},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2016,"geschlecht_code":2,"anzahl_personen":7153},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2016,"geschlecht_code":1,"anzahl_personen":7103},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2015,"geschlecht_code":2,"anzahl_personen":7125},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2015,"geschlecht_code":1,"anzahl_personen":7059}])
  const [pieValue, setPieValue] = useState([{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"nationalitaet_code":2,"nationalitaet_bezeichnung":"Ausland","geschlecht_code":2,"geschlecht_bezeichnung":"Weiblich","anzahl_personen":2460},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"nationalitaet_code":1,"nationalitaet_bezeichnung":"Schweiz","geschlecht_code":1,"geschlecht_bezeichnung":"Männlich","anzahl_personen":4868},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"nationalitaet_code":2,"nationalitaet_bezeichnung":"Ausland","geschlecht_code":1,"geschlecht_bezeichnung":"Männlich","anzahl_personen":2859},{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","jahr":2022,"nationalitaet_code":1,"nationalitaet_bezeichnung":"Schweiz","geschlecht_code":2,"geschlecht_bezeichnung":"Weiblich","anzahl_personen":5226}])
  console.log(pieValue, bfs_nr_gemeinde, jahr, gemeinde, lineValue, barValue)
  const pieData = { // Daten für das Kreisdiagramm
    labels: ["Schweizer Staatsbürgerschaft", "Ausländische Staatsbürgerschaft"],
    datasets: [
      {
        label: "Anzahl Personen",
        data: [(pieValue[0].anzahl_personen + pieValue[1].anzahl_personen), (pieValue[2].anzahl_personen + pieValue[3].anzahl_personen)],
        backgroundColor: ["rgba(255,99,132,0.2)", "rgba(54, 162, 235, 0.2)"],
        borderColor: ["rgba(255,132,1)", "rgba(54,162,235,1)"],
        borderWidth: 1,
      },
    ],
  };
  const pieOptions = { //Optionen für das Kreisdiagramm
    responsive: true,
    plugins: {
      legend: { //Legende unterhalb des Diagramms
        position: "bottom",
      }
    }
  }

  const labels = ["2015","2016","2017","2018","2019","2020","2021","2022"]; //Zietabstände der X Achse defineiren
  const barData = { //Daten für das Balkendiagramm festelgen
    labels,
    datasets: [
      {
        label: "Anzahl männlicher Bürger",
        data: [barValue[15].anzahl_personen, barValue[13].anzahl_personen, barValue[11].anzahl_personen, barValue[9].anzahl_personen, barValue[7].anzahl_personen, barValue[5].anzahl_personen, barValue[3].anzahl_personen, barValue[1].anzahl_personen],
        backgroundColor: "rgba(53, 162, 235, 0.5)"
      },
      {
        label:"Anzahl weiblicher Bürgerinnen",
        data: [barValue[14].anzahl_personen,barValue[12].anzahl_personen,barValue[10].anzahl_personen,barValue[8].anzahl_personen,barValue[6].anzahl_personen,barValue[4].anzahl_personen,barValue[2].anzahl_personen,barValue[0].anzahl_personen],
        backgroundColor: "rgba(255, 99, 132, 0.5)"
      }

    ]
  }
  const barOptions = { //Optionen für das Balkendiagramm
    responsive: true,
    plugins: {
      legend: {
        position: "bottom",
      }
    }
  }

  const lineData = { //Daten für das Linien diagramm
    labels,
    datasets: [
      {
        label: "Anzahl Personen",
        data: [lineValue[7].anzahl_personen,lineValue[6].anzahl_personen,lineValue[5].anzahl_personen,lineValue[4].anzahl_personen,lineValue[3].anzahl_personen,lineValue[2].anzahl_personen,lineValue[1].anzahl_personen,lineValue[0].anzahl_personen],
        borderColor: "rgba(255,99,132,1)",
        backgroundColor: "rgba(255,99,132,0.5)"
      }
    ]
  }
  const lineOptions = { //Optionen für das Liniendiagramm
    responsive: true,
    plugins: {
      legend: {
        position: "bottom",
      }
    }
  }
  useEffect(() => { createComboBox(setJahr, setPieValue, setBfs_nr_gemeinde, bfs_nr_gemeinde, jahr, setGemeinde)}, []);
  useEffect(() => {
    axios.get("/api/RCV/bev/" + bfs_nr_gemeinde).then((res) => {
    setLineValue(res.data)
  })
    axios.get("/api/RCV/ges/" + bfs_nr_gemeinde).then((res) => {
      setBarValue(res.data)
    })
    axios.get("/api/RCV/staat/" + bfs_nr_gemeinde +"/" + jahr).then((res) => {
      setPieValue(res.data)
    })}, [jahr, bfs_nr_gemeinde])
  return (
    <div className="mt-8" id="diagram">
      <h1 className="text-4xl" id="diagramme">
        Diagramme
      </h1>
      <div className="grid grid-cols-2 mt-2">
        <div className="block">
          <div>
        <label htmlFor="gemeinde">Gemeinde:</label>
          </div>
          <div>
        <select name="gemeinde" id="gemeindeCombo" className="pl-2"></select>
          </div>
          <div>
        <label htmlFor="jahre">Jahr:</label>
          </div>
          <div>
          <input type="range" min="2015" max="2022" defaultValue="2022" name="jahre" id="jahre" step="-1" list="jahrStepper"/>
        <datalist id={"jahrStepper"}>
          {Array.from({length: (2022 + 1 - 2015)}, (value, index) => (2015 + index)).map((e, key) => <option key={key}>{e}</option>)}
        </datalist>
            <p id="rangeValue">{jahr}</p>
          </div>
          </div>
        <div>
          <h2 id="staat" className="text-2xl">Bevölkerungszahl nach Staatsbürgerschaft der Gemeinde {gemeinde} vom Jahr {jahr}</h2>
          <Pie className="mt-4" id="pie" options={pieOptions} data={pieData} />
        </div>
      </div>
      <div className="mt-2">
        <h2 id="entwicklung" className="text-2xl">Entwicklung der Bevölkerungszahl von 2015 bis 2022 der Gemeinde {gemeinde}</h2>
        <Line id="line" options={lineOptions} data={lineData}/>
      </div>
      <div className="mt-2">
        <h2 id="geschlecht" className="text-2xl">Entwicklung der Bevölkerung nach Geschlecht von 2015 bis 2022 der Gemeinde {gemeinde}</h2>
        <Bar id="bar" options={barOptions} data={barData} />
      </div>
    </div>
  );
}

export default Charts;
