import { useEffect } from "react";
import TGsvg from "../assets/map-tg.svg";
import SVGInject from "@iconfu/svg-inject";
import axios from "axios";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);
let values = [{"bfs_nr_gemeinde":4401,"gemeinde_name":"Arbon","bezirk_name":"Bezirk Arbon","jahr":2022,"anzahl_personen_total":14184,"anzahl_personen_männlich":7059,"anzahl_personen_weiblich":7125,"anzahl_personen_männlich_schweiz":4688,"anzahl_personen_weiblich_schweiz":5092,"anzahl_personen_männlich_ausland":2371,"anzahl_personen_weiblich_ausland":2033}];


const pieData = {
  labels: [
    "Mänlicher Schweizer",
    "Weibliche Schweizerinnen",
    "Mänliche Ausländer",
    "Weibliche Ausländerinnen",
  ],
  datasets: [
    {
      label: "Anzahl Personen",
      data: [
        values[0].anzahl_personen_männlich_schweiz,
        values[0].anzahl_personen_weiblich_schweiz,
        values[0].anzahl_personen_männlich_ausland,
        values[0].anzahl_personen_weiblich_ausland,
      ],
      backgroundColor: [
        "rgba(255,99,132,0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(100, 12, 122, 0.2)",
        "rgba(180, 99, 235, 0.2)",
      ],
      borderColor: [
        "rgba(255,132,1)",
        "rgba(54,162,235,1)",
        "rgba(100, 12, 122, 1)",
        "rgba(180, 99, 235, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

function styling() {
  document
    .getElementById("municipalities") //Id welche alle Gemeinden beinflusst
    .classList.add("fill-green-600", "stroke-gray-300", "stroke-[0.5px]"); //Grün einfärben mit Grauen rändern
  let elTooltip = document.getElementById("map-hover-tooltip"); //holt das vordefinierte p Element
  let elLake = document.getElementById("lakes");
      elLake.classList.add("fill-blue-300"); //Id welche den See beeinflusst
  elLake.addEventListener("mouseover", (event) => {
    elTooltip.classList.remove("hidden"); // tooltip (p Element) wird angezeigt (durch entfernen von hidden)
    event.target.classList.add("fill-blue-100"); // der blaue wert wird heller gesetzt
    elTooltip.innerHTML = "Der Bodensee ist keine Gemeinde, sondern ein Gewässer. , klicken Sie auf eine Gemiende um mehr zu erfahren."/* Text welcher angezeigt wird*/
    elTooltip.style.top = `${event.pageY}px`; // das p Element so konfigurieren das es dem Mauszeiger folgt
    elTooltip.style.left = `${event.pageX}px`;
  });
  //mouseout event
  elLake.addEventListener("mouseout", (event) => {
    elTooltip.classList.add("hidden"); //Das p Element wird wieder versteckt
    event.target.classList.remove("fill-blue-100"); //Das hellere Grün wird entfernt
  });
}

async function hoverTooltip(values) {
  let gemeindeArr = (await axios.get("/api/RCV/getGemeinden")).data; //holt einen Array welche Javascript Objekte drin hat, mit bfs_nr und dem Gemeinden Namen
  let elPathsCountry = document.querySelectorAll("#municipalities path"); //holt jede einzelne Gemeinde
  let elTooltip = document.getElementById("map-hover-tooltip"); //holt das vordefinierte p Element

  for (let i = 0; i < elPathsCountry.length; i++) {// geht durch jede Gemeinde
    // mouseover event
    elPathsCountry[i].addEventListener("mouseover", (event) => {
      let id = event.target.id;
      elTooltip.classList.remove("hidden"); // tooltip (p Element) wird angezeigt (durch entfernen von hidden)
      event.target.classList.add("fill-green-300"); // der grün wert wird heller gesetzt
      elTooltip.innerHTML = /* Text welcher angezeigt wird*/
        gemeindeArr.filter((gemeinde) => gemeinde.bfs_nr_gemeinde === id)[0] /* Die gemeinde aus dem Gemeinde Array suchen*/
          .gemeinde_name + "</br>Um mehr zu erfahren klicken.";
      elTooltip.style.top = `${event.pageY}px`; // das p Element so konfigurieren das es dem Mauszeiger folgt
      elTooltip.style.left = `${event.pageX}px`;
    });
    //mouseout event
    elPathsCountry[i].addEventListener("mouseout", (event) => {
      elTooltip.classList.add("hidden"); //Das p Element wird wieder versteckt
      event.target.classList.remove("fill-green-300"); //Das hellere Grün wird entfernt
    });
    // click event
    elPathsCountry[i].addEventListener("click", (event) => {
      let elPopUp = document.getElementById("map-hover-popup"); //Vordefiniertes Element für das Popup holen
      let id = event.target.id; //Id des Elemnts holen auf welches geklickt wurde
      axios.get("/api/RCV/map/" + id).then((res) => { //axios abfrage auf die Gemeinde
        values = res.data; //values anpassen, vom Standartwert zu geklickter Gemeinde so das das Kreisdiagramm angepasst wird
        elPopUp.classList.remove("hidden"); //Popup anzeigen, (Entfernen von hidden class)
        let elClose = document.querySelector("#map-hover-popup button"); //Schliessen knopf holen
        elClose.addEventListener("click", () => { //klick event auf den Schliessen Knopf
          elPopUp.classList.add("hidden"); // das Popup wird wieder versteckt
        });
        const elh2 = document.querySelector("#map-hover-popup h2"); //H2 Element wird geholt
        elh2.innerHTML = values[0].gemeinde_name; //Dem H2 Element wird als Inhalt der Name der Gemeinde übergeben
        const elP = document.querySelector("#map-hover-popup p"); //P Element wird geholt
        elP.innerHTML = `Bezirk: ${values[0].bezirk_name} </br>Zahlen aus dem Jahr: ${values[0].jahr} </br> Anzahl Personen Insgesamt: ${values[0].anzahl_personen_total}`; //Dem P Element werden verschiedene Werte übergeben
      })

    });
  }
}

function Map() { // Komponente Map, sie regelt alles was mit der Karte zu tun hat
  useEffect(() => {
    SVGInject.setOptions({ makeIdsUnique: false }); //Library für die Karte
    const elMapTg = document.getElementById("tg-map");
    SVGInject(elMapTg).then(() => { //Library für die Karte
      styling(); //ruft die Funktion auf welche alles styled
      hoverTooltip(values); //ruft die funktion auf welche den Elementen die eventlistener hinzufügt
    });
  }, []);
  return (
    <div className="mt-8" id="Karte">
      <img id="tg-map" src={TGsvg} />
      <p id="map-hover-tooltip" className="hidden font-semibold absolute"></p>
      <div
        className="top-0 left-0 p-6 w-full h-full bg-slate-400 bg-opacity-30 hidden fixed justify-center content-center"
        id="map-hover-popup"
      >
        <div className="rounded p-6 pt-1 w-screen h-screen max-h-[80%] max-w-[70%] bg-slate-50 border-2 border-gray-300">
          <button className="float-right font-semibold text-2xl">X</button>
          <h2 className="text-2xl font-semibold"></h2>
          <p className="text-xl"></p>
          <Pie
            className="max-h-[50%] h-screen pt-4"
            data={pieData}
            options={{ responsive: true }}
          />
        </div>
      </div>
    </div>
  );
}

export default Map;
