import Charts from "./components/Charts";
import Footer from "./components/Footer";
import Introduction from "./components/Introduction";
import Navigation from "./components/Navigation.jsx";
import Map from "./components/Map";



function App() {
  return (
    <div className="m-8">
      <Navigation />
        <Introduction />
        <Map />
        <Charts />
      <Footer />
    </div>
  );
}

export default App;
